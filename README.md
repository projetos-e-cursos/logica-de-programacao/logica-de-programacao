# logica de programacao

## Primeiros programas com JS e HTML.

### Conceitos abordados:
- O que é HTML;
- O que é JavaScript;
- Variáveis;
- Tipos de Variáveis;
- Conversão entre tipos;
- Estruturas condicionais;
- Estruturas de repetição;
- Interagindo com o HTML usando o JS;
- querySelector();
- Eventos;
- Interagir com o usuário;